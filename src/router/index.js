import Vue from 'vue'
import Router from 'vue-router'

/** Home Page Component */
import HomePage from '@/components/Home'

/** Set Component From Meetup Directory */
import SingleMeetupPage from '@/components/Meetup/Single_meetup'
import MeetupsPage from '@/components/Meetup/Meetups'
import CreateMeetupsPage from '@/components/Meetup/Create_meetups'

/** Set Component From User Directory */
import ProfilePage from '@/components/User/Profile'
import SigninPage from '@/components/User/Sign_in'
import SignupPage from '@/components/User/Sign_up'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HomePage',
      component: HomePage
    },
    {
      path: '/meetups',
      name: 'MeetupsPage',
      component: MeetupsPage
    },
    {
      path: '/meetups/new',
      name: 'CreateMeetupsPage',
      component: CreateMeetupsPage
    },
    {
      path: '/meetups/:id',
      name: 'SingleMeetupPage',
      props: true,
      component: SingleMeetupPage
    },
    {
      path: '/user/profile',
      name: 'ProfilePage',
      component: ProfilePage
    },
    {
      path: '/user/sign_up',
      name: 'SignupPage',
      component: SignupPage
    },
    {
      path: '/user/sign_in',
      name: 'SigninPage',
      component: SigninPage
    }
  ],
  mode: 'history'
})
