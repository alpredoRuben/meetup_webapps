import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    recordDataMeetups: [
      {
        imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/4/47/New_york_times_square-terabass.jpg',
        id: 'new_york001',
        title: 'Meetup in New York, New York City, USA',
        date: '2018-07-17'
      },
      {
        imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/f/f2/Berlin%2C_Kreuzberg%2C_Wrangelstrasse_97-99%2C_Kaserne_des_3._Garde-Regiments_zu_Fuss.jpg',
        id: 'berlin001',
        title: 'Meetup in Berlin, Berlin City, Germany',
        date: '2018-07-17'
      },
      {
        imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/c/c9/Medan-post-office.jpg',
        id: 'medan001',
        title: 'Meetup in Medan, Sumatera Utara, Indonesia',
        date: '2018-07-17'
      }
    ],
    user: {
      id: 'testing01',
      registeredMeetups: ['testing0123ThisUser1']
    }
  },
  mutations: {},
  actions: {},
  getters: {
    loadingAllMeetup (state) {
      return state.recordDataMeetups.sort((paramsA, paramsB) => {
        return paramsA.date > paramsB.date
      })
    },
    featureMeetup (state, getters) {
      return getters.loadingAllMeetup.slice(0, 5)
    },
    loadingSingleMeetup (state) {
      return (findID) => {
        return state.recordDataMeetups.find((recordData) => {
          return recordData.id === findID
        })
      }
    }
  }
})
