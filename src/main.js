import Vue from 'vue'
import App from './App'
import router from './router'
import Vuetify from 'vuetify'
import colors from 'vuetify/es5/util/colors'
import { store } from './store/store'
import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuetify, {
  theme: {
    primary: colors.blue.darken1,
    secondary: colors.indigo.darken4,
    accent: colors.shades.black,
    error: colors.red.darken1,
    info: colors.cyan.lighten1,
    success: colors.green.accent3,
    warning: colors.amber.accent3
  }
})

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
